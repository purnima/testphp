#!/usr/bin/php
<?php

$shortopts = "u:p:h:";

$longopts = ["help","file:", "create_table", "dry_run"];

$opts = getopt($shortopts, $longopts);

if (isset($opts["help"])) {
    fprintf(STDERR, "Here is some help!\n --file [csv file name] – this is the name of the CSV to be parsed \n --create_table – this will cause the MySQL users table to be built (and no further action will be taken) \n --dry_run – this will be used with the --file directive in case we want to run the
script but not insert into the DB. All other functions will be executed, but the
database won't be altered \n -u – MySQL username \n -p – MySQL password \n -h – MySQL host \n --help – which will output the above list of directives with details ");
    exit;
}

$host="localhost"; // Host name.
$db_user="root"; //mysql user
$db_password=""; //mysql pass
$db='users'; // Database name.
//$conn=mysql_connect($host,$db_user,$db_password) or die (mysql_error());
//mysql_select_db($db) or die (mysql_error());
$con=mysqli_connect($host,$db_user,$db_password,$db);
// Check connection
if (mysqli_connect_errno())
{
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

if (isset($opts["create_table"])) {
    //check table exits
    $val = mysqli_query($con, 'select 1 from `user` LIMIT 1');

    if($val == FALSE) //create table if not exists
    {
       // Attempt create table query execution
        $sql = "CREATE TABLE user(

            name VARCHAR(30) NOT NULL,
            surname VARCHAR(30) NOT NULL,
            email VARCHAR(70) NOT NULL UNIQUE
        )";
        if(mysqli_query($con, $sql)){
            echo "Table created successfully.";
        } else{
            echo "ERROR: Could not able to execute $sql. " . mysqli_error($con);
        }
    }
    else{
        fprintf(STDERR, "Table already exitst!" . PHP_EOL);
    }
}

$filename ="";
if (isset($opts["file"]) && !isset($opts['dry_run'])) { //do not insert in db if dry run is used

    $filename = $opts["file"];

    if (empty($filename)) {

    fprintf(STDERR, "We wanted a file!" . PHP_EOL);
    exit(1);

    }else{

        $ext=substr($filename,strrpos($filename,"."),(strlen($filename)-strrpos($filename,".")));
        //we check,file must be have csv extension
        if($ext==".csv"){

                $file = fopen($filename, "r");
                $emapData = fgetcsv($file, 10000, ","); //The loop will start from the 2nd row in the CSV file.
                 while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE)
                 {
                    $name = ucfirst($emapData[0]); //name capitalized
                    $surname = ucfirst($emapData[1]); //surname capitalized

                    //need to validate email before inserting
                    $email = strtolower($emapData[2]); //email lowercased

                    if(!filter_var($email, FILTER_VALIDATE_EMAIL)){ //validate
                        fprintf(STDOUT, "Invalid Email %s" . PHP_EOL, $email);
                         
                    }else{
                        $sql = "INSERT into user(name,surname,email) values('$name','$surname','$email')";
                        mysqli_query($con, $sql);
                    }

                   
                 }
                 fclose($file);
                 echo "CSV File has been successfully Imported.";
            
        }else{
            fprintf(STDERR, "Please provide CSV file!" . PHP_EOL);
            fprintf(STDOUT, "File is %s" . PHP_EOL, $ext);
        }

    }
        
}